# -*- coding: utf-8 -*-

import os
import subprocess
import sys
import threading
import time

try:
    import Rockhopper.LinuxCNCWebSktSvr
except ImportError:
    with open('Rockhopper/__init__.py', 'w') as f:
        f.write('\n')

from Rockhopper.LinuxCNCWebSktSvr import main_loop as rh_main_loop


class MyPoller(Rockhopper.LinuxCNCWebSktSvr.LinuxCNCStatusPoller):
    def __init__(self, *args, **kwargs):
        self.linuxcnc_pid = 0
        Rockhopper.LinuxCNCWebSktSvr.LinuxCNCStatusPoller.__init__(self, *args, **kwargs)

    def find_linuxcnc_pid(self):
        from subprocess import check_output
        pid = int(check_output(["pidof", '-s', '-x', 'linuxcnc']))
        return pid

    def is_linuxcnc_running(self):
        if self.linuxcnc_pid == 0:
            try:
                self.linuxcnc_pid = self.find_linuxcnc_pid()
            except Exception:
                return False
        else:
            try:
                os.kill(self.linuxcnc_pid, 0)
            except OSError:
                self.reset_linuxcnc_checking()
                return False
        return True

    def reset_linuxcnc_checking(self):
        self.linuxcnc_pid = 0

    def hal_poll_init(self):
        def hal_poll_thread():
            myinstance = Rockhopper.LinuxCNCWebSktSvr.instance_number

            while myinstance == Rockhopper.LinuxCNCWebSktSvr.instance_number:

                # first, check if linuxcnc is running at all
                if not self.is_linuxcnc_running():
                    self.hal_mutex.acquire()
                    try:
                        if self.linuxcnc_is_alive:
                            print "LinuxCNC has stopped."
                        self.linuxcnc_is_alive = False
                        self.pin_dict = {}
                        self.sig_dict = {}
                    finally:
                        self.hal_mutex.release()
                    time.sleep(Rockhopper.LinuxCNCWebSktSvr.UpdateHALPollPeriodInMilliSeconds / 1000.0)
                    continue
                else:
                    if not self.linuxcnc_is_alive:
                        print "LinuxCNC has started."
                    self.linuxcnc_is_alive = True

                self.p = subprocess.Popen(['halcmd', '-s', 'show', 'pin'], stderr=subprocess.PIPE,
                                          stdout=subprocess.PIPE)
                rawtuple = self.p.communicate()
                if len(rawtuple[0]) <= 0:
                    time.sleep(Rockhopper.LinuxCNCWebSktSvr.UpdateHALPollPeriodInMilliSeconds / 1000.0)
                    continue
                raw = rawtuple[0].split('\n')

                pins = [filter(lambda a: a != '', [x.strip() for x in line.split(' ')]) for line in raw]

                # UPDATE THE DICTIONARY OF PIN INFO
                # Acquire the mutex so we don't step on other threads
                self.hal_mutex.acquire()
                try:
                    self.pin_dict = {}
                    self.sig_dict = {}

                    for p in pins:
                        if len(p) > 5:
                            # if there is a signal listed on this pin, make sure
                            # that signal is in our signal dictionary
                            self.sig_dict[p[6]] = p[3]
                        if len(p) >= 5:
                            self.pin_dict[p[4]] = p[3]
                finally:
                    self.hal_mutex.release()

                # before starting the next check, sleep a little so we don't use all the CPU
                time.sleep(Rockhopper.LinuxCNCWebSktSvr.UpdateHALPollPeriodInMilliSeconds / 1000.0)
            print "HAL Monitor exiting... ", myinstance, Rockhopper.LinuxCNCWebSktSvr.instance_number

        # Main part of hal_poll_init:
        # Create a thread for checking the HAL pins and sigs
        self.hal_mutex = threading.Lock()
        self.hal_thread = threading.Thread(target=hal_poll_thread)
        self.hal_thread.start()


def my_rh_main(ini_file):
    from optparse import OptionParser
    from Rockhopper.LinuxCNCWebSktSvr import UpdateStatusPollPeriodInMilliSeconds
    from Rockhopper.LinuxCNCWebSktSvr import application_path, readUserList, application
    from random import random
    import logging
    import tornado.autoreload

    sys.argv = [sys.argv[0], ini_file]

    def fn():
        Rockhopper.LinuxCNCWebSktSvr.instance_number = random()
        print "Webserver reloading..."

    parser = OptionParser()
    parser.add_option("-v", "--verbose", dest="verbose", default=0,
                      help="Verbosity level.  Default to 0 for quiet.  Set to 5 for max.")

    (Rockhopper.LinuxCNCWebSktSvr.options, args) = parser.parse_args()

    if (int(Rockhopper.LinuxCNCWebSktSvr.options.verbose) > 4):
        print "Options: ", Rockhopper.LinuxCNCWebSktSvr.options
        print "Arguments: ", args[0]

    Rockhopper.LinuxCNCWebSktSvr.instance_number = random()
    Rockhopper.LinuxCNCWebSktSvr.LINUXCNCSTATUS = MyPoller(rh_main_loop, UpdateStatusPollPeriodInMilliSeconds)

    if (int(Rockhopper.LinuxCNCWebSktSvr.options.verbose) > 4):
        print "Parsing INI File Name"

    if len(args) < 1:
        sys.exit('Usage: LinuxCNCWebSktSvr.py <LinuxCNC_INI_file_name>')
    Rockhopper.LinuxCNCWebSktSvr.INI_FILENAME = args[0]
    [Rockhopper.LinuxCNCWebSktSvr.INI_FILE_PATH, x] = os.path.split(Rockhopper.LinuxCNCWebSktSvr.INI_FILENAME)

    if (int(Rockhopper.LinuxCNCWebSktSvr.options.verbose) > 4):
        print "INI File: ", Rockhopper.LinuxCNCWebSktSvr.INI_FILENAME

    logging.basicConfig(filename=os.path.join(application_path, 'linuxcnc_webserver.log'),
                        format='%(asctime)sZ pid:%(process)s module:%(module)s %(message)s', level=logging.ERROR)

    # rpdb2.start_embedded_debugger("password")

    readUserList()

    logging.info("Starting linuxcnc http server...")
    print "Starting Rockhopper linuxcnc http server."

    # see http://www.akadia.com/services/ssh_test_certificate.html to learn how to generate a new server SSL certificate
    # for httpS protocol:
    # application.listen(8000, ssl_options=dict(
    #    certfile="server.crt",
    #    keyfile="server.key",
    #    ca_certs="/etc/ssl/certs/ca-certificates.crt",
    #    cert_reqs=ssl.CERT_NONE) )

    # for non-httpS (plain old http):
    application.listen(8000)

    # cause tornado to restart if we edit this file.  Usefull for debugging
    tornado.autoreload.add_reload_hook(fn)
    tornado.autoreload.start()

    # start up the webserver loop
    rh_main_loop.start()
