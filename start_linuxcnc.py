#!/usr/bin/env python
# -*- coding: utf-8 -*-

# { "id":"Login", "user": "default", "password": "default" }
# { "id":"SystemStart", "command":"put", "name":"startup" }

import json

from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory
from twisted.internet import reactor, defer


class MyWSProtocol(WebSocketClientProtocol):
    def onConnect(self, response):
        print("Connected to Rockhopper server: {0}".format(response.peer))

    def onOpen(self):
        print("WebSocket connection open.")

        def login(_):
            print "Logging in..."
            self.sendMessage(json.dumps({"id": "Login", "user": "default", "password": "default"}))

        def start_linuxcnc(_):
            print "Starting linuxcnc..."
            self.sendMessage(json.dumps({"id": "SystemStart", "command": "put", "name": "startup"}))

        def close(_):
            self.sendClose()
            reactor.callLater(3, reactor.stop)

        d = defer.Deferred()
        d.addCallback(login)
        d.addCallback(start_linuxcnc)
        d.addBoth(close)

        self.factory.reactor.callLater(1, lambda: d.callback(None))

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            print("Text message received: {0}".format(payload.decode('utf8')))

    def onClose(self, wasClean, code, reason):
        print("Rockhopper connection closed {0}".format('clean' if wasClean else 'unexpected'))


if __name__ == "__main__":
    print "Starting linuxcnc"
    factory = WebSocketClientFactory(u"ws://localhost:8000/websocket/linuxcnc", protocols=['linuxcnc'])
    factory.protocol = MyWSProtocol

    reactor.connectTCP("localhost", 8000, factory)
    reactor.run()
    print "LinuxCNC started"
