# -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod


def extract_axis_name(axis):
    for ax in ('X', 'Y', 'Z', 'A', 'B'):
        if ax in axis:
            return ax
    raise KeyError('Unknown axis: {}'.format(axis))


class IFollowHook:
    """ Интерфейс состояния окна """

    __metaclass__ = ABCMeta

    @abstractmethod
    def changed(self, name, old_value, new_val):
        """ Найстройка изменена """


class FollowXYHook(IFollowHook):
    def __init__(self, controller):
        self.controller = controller

    def changed(self, name, old_value, new_val):
        c = {extract_axis_name(name): new_val}
        self.controller.move_to_nonblock(**c)


class FollowAxisHook(IFollowHook):
    def __init__(self, controller, axis='Z'):
        self.axis = axis
        self.controller = controller

    def changed(self, name, old_value, new_val):
        c = {self.axis: new_val}
        self.controller.move_to_nonblock(**c)

