# -*- coding: utf-8 -*-

import gobject
import gtk
import os
from abc import ABCMeta, abstractmethod

from FollowHooks import FollowXYHook, FollowAxisHook
from Point import Point

button7 = 'Upper-left button'
button4 = 'Mid-left button'
button1 = 'Down-left button'
button9 = 'Upper-right button'
button6 = 'Mid-right button'
button3 = 'Down-right button'

btn2_gtk_btn_id_map = {
    button7: 'button00',
    button9: 'button01',
    button4: 'button10',
    button6: 'button11',
    button1: 'button20',
    button3: 'button21',
}

btn2_pin_map = {
    button7: 7,
    button9: 9,
    button4: 4,
    button6: 6,
    button1: 1,
    button3: 3,
}

buttons = btn2_gtk_btn_id_map.keys()

cassete_1_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img/cassete-1.png')
cassete_2_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img/cassete-2.png')

cassete_1_pixbuf_400x270 = gtk.gdk.pixbuf_new_from_file_at_size(cassete_1_path, 400, 270)
cassete_2_pixbuf_400x270 = gtk.gdk.pixbuf_new_from_file_at_size(cassete_2_path, 400, 270)
cassete_1_pixbuf_520x420 = gtk.gdk.pixbuf_new_from_file_at_size(cassete_1_path, 520, 420)
cassete_2_pixbuf_520x420 = gtk.gdk.pixbuf_new_from_file_at_size(cassete_2_path, 520, 420)


class IGuiState:
    """ Интерфейс состояния окна """

    __metaclass__ = ABCMeta

    _work_modes_template = u"<span size='x-large' foreground='#{}'>{}</span>"

    work_modes_table = (
        _work_modes_template.format('0091ea', 'Две стороны - две кассеты'),
        _work_modes_template.format('aa00ff', 'Две стороны - одна кассета'),
        _work_modes_template.format('00b0ff', 'Одна сторона - две кассеты'),
        _work_modes_template.format('d500f9', 'Одна сторона - одна кассета'),
        _work_modes_template.format('40c4ff', 'Заливка - две кассеты'),
        _work_modes_template.format('e040fb', 'Заливка - одна кассета'),
    )

    @abstractmethod
    def apply(self, gui):
        """ Применить состояние к окну """

    def release(self, gui):
        """ Отменить изменения """
        gui.commit_changes()

    @abstractmethod
    def update_screen(self, gui):
        """ Обновить экран """

    @staticmethod
    def common_apply(data, gui):
        for btn in data:
            info = data[btn]
            gui.set_button_text(btn, info['text'])
            if 'is_tocl' in info:
                gui.set_button_toggle_on_click(btn, info['is_tocl'])
            else:
                gui.set_button_toggle_on_click(btn, False)
            gui.set_button_enabled(btn, True)
            callback_info = info['callback_info']
            if callback_info:
                btn_callback, btn_cookie = callback_info
                gui.set_button_callback(btn, gui[btn_callback], btn_cookie)

    @staticmethod
    def create_gtk_label():
        label = gtk.Label()
        label.set_justify(gtk.JUSTIFY_CENTER)
        label.show()
        return label


class DisabledState(IGuiState):
    def __init__(self):
        self.widgets = []
        self.gui = None

        w = IGuiState.create_gtk_label()
        w.set_markup(u"""<span size='xx-large' foreground='#0091ea'>Установка аэрозольного напыления фоторезиста</span>
<span size='large'>Для включения установки нажмите кнопку "Включить"</span>""")
        self.widgets.append((w, Point(50, 100)))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()

        gui.set_button_text(button7, u'Выключить установку')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self._machine_poweroff, None)

        gui.set_button_text(button9, u'Включить')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self._machine_prepare, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.reconnect()
        gui.hold_interface(None)

    def _machine_poweroff(self, _):
        self.gui.machine_start_program("SystemShutdown", None)

    def _machine_prepare(self, _):
        self.gui.machine_start_program("Prepare", lambda: self.gui.switch_state("HOME_SCREEN"))

    def update_screen(self, gui):
        pass


class HomeScreenState(IGuiState):
    work_mode = 'Work Mode'

    state_defs = {
        button4: {'text': u'Режим', 'callback_info': ('change_settings', work_mode)},
        button1: {'text': u'Промывка', 'callback_info': ('switch_state', "WASHING"), 'is_tocl': True},
        button9: {'text': u'Старт', 'callback_info': ('switch_state', "WORKING")},
        button3: {'text': u'Настройки', 'callback_info': ('switch_state', "SETTINGS_MENU_P1")}
    }

    params_list_template = u"<span size='medium' foreground='#2962ff'>{}</span>"

    params_list = (
        params_list_template.format(u'Начало рабочей области, мм:'),
        params_list_template.format(u'Размер рабочей зоны (X:Y), мм:'),
        params_list_template.format(u'Подъём (H), мм:'),
        params_list_template.format(u'Расход (Q), мм:'),
        params_list_template.format(u'Шаг между строками (T), мм:'),
        params_list_template.format(u'Скорость рабочего хода, мм/мин:'),
    )

    params2key_table = {
        params_list[0]: ('_X0', '_Y0'),
        params_list[1]: ('_X', '_Y'),
        params_list[2]: '_H',
        params_list[3]: '_Q',
        params_list[4]: '_T',
        params_list[5]: '_v',
    }

    def __init__(self):
        self.gui = None
        self.widgets = []
        w = IGuiState.create_gtk_label()
        w.set_markup(
            u"""<span size='xx-large' foreground='#0091ea'>Установка аэрозольного напыления фоторезиста</span>""")
        self.widgets.append((w, Point(-0.5, 30)))

        self.mode_label = IGuiState.create_gtk_label()
        self.widgets.append((self.mode_label, Point(70, -0.2)))

        self.image_1 = gtk.image_new_from_pixbuf(cassete_1_pixbuf_400x270)
        self.image_2 = gtk.image_new_from_pixbuf(cassete_2_pixbuf_400x270)
        self.widgets.append((self.image_1, Point(-0.28, -0.6)))
        self.widgets.append((self.image_2, Point(-0.28, -0.6)))

        self._params_table = gtk.Table(6, 2)
        self._params_table.show()
        self._params_table.set_col_spacing(0, 20)
        for i in xrange(6):
            self._params_table.set_row_spacing(i, 15)
        self.widgets.append((self._params_table, Point(-0.705, -0.56)))

        self._params_table_values = {}

        row = 0
        for name in HomeScreenState.params_list:
            description = IGuiState.create_gtk_label()
            description.set_markup(name)
            description.set_alignment(1, .5)
            self._params_table.attach(description, 0, 1, row, row + 1)
            value = IGuiState.create_gtk_label()
            value.set_alignment(0, .5)
            self._params_table.attach(value, 1, 2, row, row + 1)
            row += 1
            self._params_table_values[value] = HomeScreenState.params2key_table[name]

    def _start_edit_consumption(self, dummy=0):
        key = 'mode_{}_Q'.format(self.gui.get_settings_value('Work Mode'))
        self.gui.edit_single_value((key, 0.01, "HOME_SCREEN",
                                    u"<span size='xx-large'>Величина открытия аэрографа</span>",
                                    u"мм"))

    def _shutdown(self, dummy=0):
        self.gui.machine_start_program("DisableMachine", lambda: self.gui.switch_state("DISABLED"))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()

        IGuiState.common_apply(HomeScreenState.state_defs, self.gui)

        self.gui.set_button_text(button7, u'Выключить')
        self.gui.set_button_enabled(button7, True)
        self.gui.set_button_callback(button7, self._shutdown, None)

        self.gui.set_button_text(button6, u'Расход (Q)')
        self.gui.set_button_enabled(button6, True)
        self.gui.set_button_callback(button6, self._start_edit_consumption, None)

        self.gui.clear_central_field()
        self.gui.add_central_field(self.widgets)
        self.update_screen(self.gui)

    def update_screen(self, gui):
        work_mode = gui.get_settings_value('Work Mode')
        mode_name = IGuiState.work_modes_table[work_mode]
        self.mode_label.set_markup(
            u"<span size='x-large'>Режим работы: </span>{}".format(
                mode_name))

        if work_mode % 2 == 1:
            self.image_1.show()
            self.image_2.hide()
        else:
            self.image_2.show()
            self.image_1.hide()

        for lbl in self._params_table_values:
            param = self._params_table_values[lbl]
            if param == '_v':
                key = 'G1 speed'
            elif isinstance(param, tuple):
                key0 = 'mode_{}{}'.format(work_mode, param[0])
                key1 = 'mode_{}{}'.format(work_mode, param[1])
                lbl.set_markup("<span>{}:{}</span>".format(
                    gui.get_settings_value(key0), gui.get_settings_value(key1)))
                continue
            else:
                key = 'mode_{}{}'.format(work_mode, param)
            lbl.set_markup("<span>{}</span>".format(gui.get_settings_value(key)))


class WorkingState(IGuiState):
    def apply(self, gui):
        gui.reset_buttons(button7)
        gui.set_button_text(button7, u'Остановить')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_working, gui)

        gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', self.on_program_finished, gui)

        gui.machine_start_program('Work', None)

    def update_screen(self, gui):
        pass

    def cancel_working(self, gui):
        gui.machine_interrupt_program(lambda: gui.switch_state("HOME_SCREEN"))

    def on_program_finished(self, gui):
        gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', None)
        gui.switch_state("HOME_SCREEN")


class WashingControlState(IGuiState):
    timer_tick = 0.1

    def __init__(self):
        self.washing_timer = 0
        self.gui = None
        self.timer_id = None

        self.widgets = []

        self.washing_prompt = IGuiState.create_gtk_label()
        self.washing_prompt.set_markup(u"<span size='xx-large'>Идет промывка аэрографа</span>")
        self.widgets.append((self.washing_prompt, Point(-0.5, 150)))

        self.timer_display = IGuiState.create_gtk_label()
        self.update_screen(0)
        self.widgets.append((self.timer_display, Point(-0.5, -0.5)))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons(button1)
        gui.set_button_enabled(button1, True)
        gui.set_button_toggle_on_click(button1, True)
        gui.set_button_text(button1, u'Остановить промывку')
        gui.set_button_callback(button1, self.cancel_washing, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)
        self.washing_timer = gui.get_settings_value('Forced washing timeout')
        gui.machine_start_program('Wash', self._start_wash_timer)

    def _start_wash_timer(self):
        self.timer_id = gobject.timeout_add(int(WashingControlState.timer_tick * 1000), self.washing_tick)

    def cancel_washing(self, dummy=0):
        if self.timer_id:
            gobject.source_remove(self.timer_id)
            self.timer_id = None
        self.gui.machine_interrupt_program(lambda: self.gui.switch_state("HOME_SCREEN"))

    def washing_tick(self):
        self.washing_timer -= WashingControlState.timer_tick
        self.update_screen(self.gui)
        if self.washing_timer <= 0:
            self.timer_id = None
            self.cancel_washing()
            return False  # stop timer
        else:
            return True  # do not stop timer

    def update_screen(self, gui):
        self.timer_display.set_markup(u"""<span size='x-large'>До окончания:</span>
<span size='xx-large' foreground='#c62828'>{0:.1f} </span><span size='x-large'>с</span>""".format(
            self.washing_timer
        ))


class SettingsControlStateP1(IGuiState):
    state_defs = {
        button1: {'text': u'Диагностика', 'callback_info': ('switch_state', "DIAG_MENU_P1")},
        button3: {'text': u'Далее >>', 'callback_info': ('switch_state', "SETTINGS_MENU_P2")}
    }

    def __init__(self):
        self.widgets = []
        self.gui = None

        self._header = IGuiState.create_gtk_label()
        self._header.set_markup(u"<span size='xx-large' foreground='#0091ea'>Настройки</span>")
        self.widgets.append((self._header, Point(-0.5, 30)))

        self._mode_label = IGuiState.create_gtk_label()
        self._mode_label.set_markup(IGuiState.work_modes_table[0])
        self.widgets.append((self._mode_label, Point(-0.5, 60)))

        self.image_1 = gtk.image_new_from_pixbuf(cassete_1_pixbuf_520x420)
        self.image_2 = gtk.image_new_from_pixbuf(cassete_2_pixbuf_520x420)
        self.widgets.append((self.image_1, Point(-0.5, -0.6)))
        self.widgets.append((self.image_2, Point(-0.5, -0.6)))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()
        IGuiState.common_apply(SettingsControlStateP1.state_defs, gui)

        gui.set_button_text(button7, u'Начало рабочей области')
        gui.set_button_enabled(button7, True)
        mode = gui.get_settings_value('Work Mode')
        gui.set_button_callback(button7, gui.edit_double_value,
                                ('mode_{}_X0'.format(mode), 1,
                                 'mode_{}_Y0'.format(mode), 1,
                                 'SETTINGS_MENU_P1',
                                 u"<span size='x-large'>Установка начала рабочей области (X0 : Y0)</span>",
                                 u'мм', u'мм', FollowXYHook(gui)))

        gui.set_button_text(button9, u'Размер рабочей области (X:Y)')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, gui.edit_double_value,
                                ('mode_{}_X'.format(mode), 1,
                                 'mode_{}_Y'.format(mode), 1,
                                 'SETTINGS_MENU_P1',
                                 u"<span size='x-large'>Установка размера рабочей области (X : Y)</span>",
                                 u'мм', u'мм', FollowXYHook(gui)))

        gui.set_button_text(button4, u'Время начальной продувки')
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, gui.edit_single_value,
                                ('Startup washing time', 0.1, 'SETTINGS_MENU_P1',
                                 u"<span size='x-large'>Время продувки аэрографа перед началом напыления</span>",
                                 u'с'))

        if mode in (0, 1):
            gui.set_button_text(button6, u'Время промежуточной продувки')
            gui.set_button_enabled(button6, True)
            gui.set_button_callback(button6, gui.edit_single_value,
                                    ('mode_{}_cwt'.format(mode), 0.1, 'SETTINGS_MENU_P1',
                                     u"<span size='x-large'>Время продувки аэрографа при перевороте "
                                     u"кассеты</span>",
                                     u'с'))

        gui.clear_central_field()
        self._update_mode(mode)
        self._update_image(mode)
        gui.add_central_field(self.widgets)

    def _update_image(self, work_mode):
        if work_mode % 2 == 1:
            self.image_1.show()
            self.image_2.hide()
        else:
            self.image_2.show()
            self.image_1.hide()

    def _update_mode(self, work_mode):
        self._mode_label.set_markup(u"<span size='x-large'>Для режима: </span>{}".format(
            IGuiState.work_modes_table[work_mode]))

    def update_screen(self, gui):
        pass


class SettingsControlStateP2(SettingsControlStateP1):
    state_defs = {
        button7: dict(text=u'Скорость',
                      callback_info=('edit_single_value',
                                     ('G1 speed', 1, "SETTINGS_MENU_P2",
                                      u"<span size='x-large'>Скорость рабочего хода при напылении</span>", u'мм/мин'))),
        button6: dict(text=u'Время промывки',
                      callback_info=('edit_single_value',
                                     ('Forced washing timeout', 1, "SETTINGS_MENU_P2",
                                      u"<span size='x-large'>Время принудительной промывки аэрографа</span>",
                                      u'с'))),
        button3: {'text': u'Выход', 'callback_info': ('switch_state', "HOME_SCREEN")}
    }

    def __init__(self):
        SettingsControlStateP1.__init__(self)

    def apply(self, gui):
        gui.reset_buttons()
        IGuiState.common_apply(SettingsControlStateP2.state_defs, gui)

        mode = gui.get_settings_value('Work Mode')

        gui.set_button_text(button1, u'Высота (H)')
        gui.set_button_enabled(button1, True)
        gui.set_button_callback(button1, gui.edit_single_value,
                                ('mode_{}_H'.format(mode), 1, 'SETTINGS_MENU_P2',
                                 u"<span size='x-large'>Высота конуса распыляемого фоторезиста</span>",
                                 u'мм', FollowAxisHook(gui, 'Z')))
        gui.set_button_text(button9, u'Шаг между строками (T)')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, gui.edit_single_value,
                                ('mode_{}_T'.format(mode), 1, 'SETTINGS_MENU_P2',
                                 u"<span size='x-large'>Шаг между строками наносимого фоторезиста</span>",
                                 u'мм'))

        gui.set_button_text(button4, u'Место продувки')
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, gui.edit_double_value,
                                ('Washing place X', 1, 'Washing place Y', 1, "SETTINGS_MENU_P2",
                                 u"<span size='x-large'>Положение ёмкости для продувки (X : Y)</span>",
                                 u'мм', u'мм', FollowXYHook(gui)))

        gui.clear_central_field()
        self._update_mode(mode)
        self._update_image(mode)
        gui.add_central_field(self.widgets)

    def update_screen(self, gui):
        pass


class DiagnosticControlStateP1(IGuiState):
    state_defs = {
        button3: {'text': u'Далее >>', 'callback_info': ('switch_state', "DIAG_MENU_P2")}
    }

    def apply(self, gui):
        gui.reset_buttons()
        IGuiState.common_apply(DiagnosticControlStateP1.state_defs, gui)

        gui.set_button_text(button7, u'Ось X')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, gui.diag_interface,
                                ('X', 1, 'DIAG_MENU_P1', u"<span size='x-large'>Ручное перемещение оси X</span>",
                                 FollowXYHook(gui), u'мм'))

        gui.set_button_text(button4, u'Ось Y')
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, gui.diag_interface,
                                ('Y', 1, 'DIAG_MENU_P1', u"<span size='x-large'>Ручное перемещение оси Y</span>",
                                 FollowXYHook(gui), u'мм'))

        gui.set_button_text(button1, u'Ось Z')
        gui.set_button_enabled(button1, True)
        gui.set_button_callback(button1, gui.diag_interface,
                                ('Z', 1, 'DIAG_MENU_P1', u"<span size='x-large'>Ручное перемещение оси Z</span>",
                                 FollowAxisHook(gui, 'Z'), u'мм'))

        gui.set_button_text(button9, u'Аэрограф')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, gui.diag_interface,
                                ('B', 0.5, 'DIAG_MENU_P1', u"<span size='x-large'>Ручное открытие аэрографа</span>",
                                 FollowAxisHook(gui, 'B'), u'мм'))

        gui.set_button_text(button6, u'Переворот')
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, gui.diag_interface,
                                ('A', 1, 'DIAG_MENU_P1', u"<span size='x-large'>Ручное управление переворотом</span>",
                                 FollowAxisHook(gui, 'A'), u'°'))

        gui.clear_central_field()

    def update_screen(self, gui):
        pass


class DiagnosticControlStateP2(IGuiState):
    state_defs = {
        button3: {'text': u'Выход', 'callback_info': ('switch_state', "SETTINGS_MENU_P1")}
    }

    def __init__(self):
        self.klapan_state = False
        self.gui = None

    def apply(self, gui):
        self.klapan_state = False
        self.gui = gui

        gui.reset_buttons()
        IGuiState.common_apply(DiagnosticControlStateP2.state_defs, gui)

        gui.set_button_text(button7, u'Клапан')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.Kalpan_toggle, None)

    def Kalpan_toggle(self, _):
        self.klapan_state = not self.klapan_state
        self.gui.output_control_nonblock({0: self.klapan_state})

    def update_screen(self, gui):
        pass


class EditorButtons:
    def apply(self, gui):
        gui.reset_buttons()

        gui.set_button_text(button4, u'- {}'.format(self.step))
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self.change_value, -self.step)
        gui.set_button_repeat_on_holding(button4, True)

        gui.set_button_text(button6, u'+ {}'.format(self.step))
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, self.change_value, self.step)
        gui.set_button_repeat_on_holding(button6, True)

    def change_value(self, step):
        pass


def gen_value_markup(step):
    acuracy = int(('%E' % step).split('E')[1])
    if acuracy < 0:
        return u"<span size='xx-large'>{} </span><span size='large'>{}</span>".format(
            '{:.' + str(-acuracy) + 'f}', '{}')
    else:
        return u"<span size='xx-large'>{:.0f} </span><span size='large'>{}</span>"


class SingleValueEditor(IGuiState, EditorButtons):
    def __init__(self, value_name, step, parent_state, editor_prompt, units='', hook=None):
        self.hook = hook
        self.value_name = value_name
        self.step = step
        self.parent_state = parent_state
        self.gui = None
        self.units = units

        self.widgets = []

        self.editor_prompt = IGuiState.create_gtk_label()
        self.editor_prompt.set_markup(editor_prompt)
        self.value_display = IGuiState.create_gtk_label()

        self.markup = gen_value_markup(step)

        self.update_screen(None)
        self.widgets.append((self.editor_prompt, Point(-0.5, 150)))
        self.widgets.append((self.value_display, Point(-0.5, -0.5)))

    def apply(self, gui):
        self.gui = gui

        EditorButtons.apply(self, gui)

        gui.set_button_text(button7, u'Отмена')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_edit, gui.get_settings_value(self.value_name))

        gui.set_button_text(button9, u'ОК')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

        self.update_screen(gui)

    def cancel_edit(self, value):
        self.gui.set_settings_value(self.value_name, value)
        self.close_editor()

    def close_editor(self, dummy=0):
        self.gui.switch_state(self.parent_state)

    def change_value(self, step):
        self.gui.change_settings((self.value_name, step))

    def update_screen(self, gui):
        v = gui.get_settings_value(self.value_name) if gui else 100

        self.value_display.set_markup(self.markup.format(
            v, self.units))

    def release(self, gui):
        gui.install_settings_change_hook(None)


class DoubleValueEditor(IGuiState):
    def __init__(self, value1_name, step1, value2_name, step2, parent_state, editor_prompt, units1='', units2='',
                 hook=None):
        self.hook = hook
        self.value1_name = value1_name
        self.step1 = step1
        self.units1 = units1
        self.value2_name = value2_name
        self.step2 = step2
        self.units2 = units2

        self.parent_state = parent_state
        self.gui = None

        self.widgets = []

        self.editor_prompt = IGuiState.create_gtk_label()
        self.editor_prompt.set_markup(editor_prompt)
        self.value1_display = IGuiState.create_gtk_label()
        self.value2_display = IGuiState.create_gtk_label()

        self.markup1 = gen_value_markup(step1)
        self.markup2 = gen_value_markup(step2)

        self.update_screen(None)
        self.widgets.append((self.editor_prompt, Point(-0.5, 150)))
        self.widgets.append((self.value1_display, Point(-0.5, -0.5)))
        self.widgets.append((self.value2_display, Point(-0.5, -0.83)))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()

        gui.set_button_text(button7, u'Отмена')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_edit,
                                (gui.get_settings_value(self.value1_name),
                                 gui.get_settings_value(self.value2_name)))

        gui.set_button_text(button9, u'ОК')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.set_button_text(button4, u'- {}'.format(self.step1))
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self.change_value1, -self.step1)
        gui.set_button_repeat_on_holding(button4, True)

        gui.set_button_text(button6, u'+ {}'.format(self.step1))
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, self.change_value1, self.step1)
        gui.set_button_repeat_on_holding(button6, True)

        gui.set_button_text(button1, u'- {}'.format(self.step1))
        gui.set_button_enabled(button1, True)
        gui.set_button_callback(button1, self.change_value2, -self.step1)
        gui.set_button_repeat_on_holding(button1, True)

        gui.set_button_text(button3, u'+ {}'.format(self.step2))
        gui.set_button_enabled(button3, True)
        gui.set_button_callback(button3, self.change_value2, self.step2)
        gui.set_button_repeat_on_holding(button3, True)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

        self.update_screen(gui)

    def cancel_edit(self, val):
        self.gui.set_settings_value(self.value1_name, val[0])
        self.gui.set_settings_value(self.value2_name, val[1])
        self.close_editor()

    def close_editor(self, dummy=0):
        self.gui.switch_state(self.parent_state)

    def change_value1(self, step):
        self.gui.change_settings((self.value1_name, step))

    def change_value2(self, step):
        self.gui.change_settings((self.value2_name, step))

    def update_screen(self, gui):
        if not gui:
            v1 = 100
            v2 = 100
        else:
            v1 = gui.get_settings_value(self.value1_name)
            v2 = gui.get_settings_value(self.value2_name)

        self.value1_display.set_markup(self.markup1.format(v1, self.units1))
        self.value2_display.set_markup(self.markup2.format(v2, self.units2))

    def release(self, gui):
        IGuiState.release(self, gui)
        gui.install_settings_change_hook(None)


class AxisMover(SingleValueEditor, EditorButtons):
    def __init__(self, axis_name, step, parent_state, prompt, follower, units=''):
        self.axis_name = axis_name
        self.follower = follower
        self.newpos = None
        super(AxisMover, self).__init__(None, step, parent_state, prompt, units)

    def apply(self, gui):
        self.gui = gui

        EditorButtons.apply(self, gui)

        gui.set_button_text(button9, u'Назад')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.set_button_text(button7, u'Домой')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self._home_axis, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

        self.update_screen(gui)

    def change_value(self, step):
        current_pos = self.gui.get_current_pos_by_axis(self.axis_name)
        self.newpos = round((current_pos + step) * 1000) / 1000.0
        self.follower.changed(self.axis_name, current_pos, self.newpos)
        self.update_screen(self.gui)

    def _home_axis(self, _):
        self.gui.home_axis_nonblock(self.axis_name)

    def update_screen(self, gui):
        if self.newpos is not None:
            v = self.newpos
        elif gui:
            v = gui.get_current_pos_by_axis(self.axis_name)
        else:
            v = 100

        self.value_display.set_markup(self.markup.format(
            v, self.units))
