#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from gui import Gui
from settings import Settings


def main():
    with Settings() as settings:
        _gui = Gui(settings)
        _gui.show()
        _gui.on_window_closed(window_closed)
        gtk.main()


def window_closed(sender):
    gtk.main_quit()


if __name__ == "__main__":
    main()
