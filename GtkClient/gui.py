# -*- coding: utf-8 -*-

import copy
import inspect

from GuiStates import *
from ToggleButton import ToggleButton
from Wachers import HWButtonWacher, PositionWatcher, UnknownWatcher, InterpretatorState

xml_name = os.path.join(os.path.dirname(os.path.realpath(__file__)), "3dp-napilenie.glade")


class Gui(gobject.GObject):
    """Этот класс должен
    1. Создавать ГУИ из glade-файла
    2. По сигналу менять его на один из стейтов
    3. Сигнализировать о событиях от кнопок
    4. Управлять видом кнопок (нажата/отжата)
    """

    __gsignals__ = {
        'button_clicked': (gobject.SIGNAL_RUN_LAST,  # default
                           gobject.TYPE_NONE,  # return value
                           (gobject.TYPE_GSTRING,)  # parameters (button_name, )
                           ),
        'state_changed': (gobject.SIGNAL_RUN_LAST,  # default
                          gobject.TYPE_NONE,  # return value
                          (gobject.TYPE_GSTRING,)  # parameters (state name, )
                          ),

        'process_command_req': (gobject.SIGNAL_RUN_LAST,
                                gobject.TYPE_NONE,
                                (gobject.TYPE_GSTRING,  # name
                                 gobject.TYPE_PYOBJECT)  # done callback
                                ),
        'reconnect': (gobject.SIGNAL_RUN_LAST,
                      gobject.TYPE_NONE,
                      ()
                      ),
    }

    states_dict = {
        'DISABLED': DisabledState(),
        'HOME_SCREEN': HomeScreenState(),
        'WORKING': WorkingState(),
        'WASHING': WashingControlState(),
        'SETTINGS_MENU_P1': SettingsControlStateP1(),
        'SETTINGS_MENU_P2': SettingsControlStateP2(),
        'DIAG_MENU_P1': DiagnosticControlStateP1(),
        'DIAG_MENU_P2': DiagnosticControlStateP2(),
    }

    states = states_dict.keys()

    def __init__(self, settings):
        gobject.GObject.__init__(self)
        self.settings = settings
        self.builder = gtk.Builder()
        self.builder.add_from_file(xml_name)
        self.window = self.builder.get_object("window1")
        self.window.set_decorated(False)
        self._central_field = self.builder.get_object("central_field")
        self._after_unblock_interface = None

        self.btns_pointers = {}
        self.create_buttons()

        self._status_bar = self.builder.get_object("statusbar")

        self.state = Gui.states_dict['DISABLED']
        self.working_mode = 0
        self.consumption = 0

        self.hold_window = gtk.Dialog(parent=self.window)

        self.setup_progress_window()

        self.watchers = {}
        self.create_watchers()

        self._settings_change_hoock = None

    def create_watchers(self):
        self.watchers = {
            u'WATCH_actual_position': PositionWatcher(self._status_bar),
            u'WATCH_din': HWButtonWacher(self.btns_pointers, btn2_pin_map),
            u'WATCH_interp_state': InterpretatorState(),
        }

    def setup_progress_window(self):
        self.hold_window.set_decorated(False)
        self.hold_window.set_keep_above(True)
        self.hold_window.set_modal(True)

        # transparent background
        # def _expose(widget, event):
        #    cr = widget.window.cairo_create()
        #    # Start drawing
        #    cr.set_operator(cairo.OPERATOR_SOURCE)
        #    cr.set_source_rgba(.8, .8, .8, 0.0)
        #    cr.rectangle(0, 0, *widget.get_size())
        #    cr.fill()
        # self.hold_window.connect('expose-event', _expose)
        self.hold_window.set_app_paintable(True)
        v_box = self.hold_window.vbox
        v_box.show()
        gif_animation = gtk.gdk.PixbufAnimation(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "img/oie_2215220iO6nAAOe.gif"))
        image = gtk.Image()
        image.set_from_animation(gif_animation)
        image.show()
        v_box.add(image)
        lbl = gtk.Label()
        lbl.set_markup(u"<span size='xx-large' foreground='red'>Подождите...</span>")
        lbl.set_justify(gtk.JUSTIFY_CENTER)
        v_box.add(lbl)
        lbl.show()
        self.hold_window.set_position(gtk.WIN_POS_CENTER_ALWAYS)

    def create_buttons(self):
        for btn in btn2_gtk_btn_id_map:
            btn_name = btn2_gtk_btn_id_map[btn]
            button = self.builder.get_object(btn_name)
            self.btns_pointers[btn] = ToggleButton(button)

    def show(self):
        self.window.show()

    def connected(self, sender, is_connected):
        if is_connected:
            self.machine_start_program('Startup', None)

    def _set_gui_state(self, state):
        if state not in Gui.states:
            raise Exception('Unknown target state: {}'.format(state))
        self.state.release(self)
        self.state = state
        self.state.apply(self)
        self.emit('state_changed', state)

    def reset(self):
        self.working_mode = 0
        self.consumption = 0
        self.state = Gui.states_dict['DISABLED']
        self.state.apply(self)

    def btn(self, button):
        return self.btns_pointers[button]

    def set_button_text(self, button, text):
        self.btn(button).set_label(text)

    def set_button_visible(self, button, is_visible):
        self.btn(button).set_visible(is_visible)

    def set_button_enabled(self, button, is_enabled):
        self.btn(button).set_sensitive(is_enabled)

    def set_button_toggle_on_click(self, button, is_toggle_on_click):
        self.btn(button).set_toggle_on_click(is_toggle_on_click)

    def set_button_callback(self, button, callback, cookie=None):
        self.btn(button).set_on_click(callback, cookie)

    def force_button_pushed_state(self, button, state):
        self.btn(button).force_pushed_state(state)

    def set_button_repeat_on_holding(self, button, is_repeat_on_holding):
        self.btn(button).set_repeat_on_holding(is_repeat_on_holding)

    def reset_buttons(self, skip=()):
        for btn in buttons:
            if btn in skip:
                continue
            self.set_button_text(btn, '')
            self.set_button_enabled(btn, False)
            self.set_button_callback(btn, None)
            self.set_button_toggle_on_click(btn, False)
            self.force_button_pushed_state(btn, False)
            self.set_button_repeat_on_holding(btn, False)

    def switch_state(self, target_state_name):
        if target_state_name not in Gui.states:
            raise KeyError('State "{}" is unknown'.format(target_state_name))
        print('Switching to state "{}"'.format(target_state_name))
        self.state.release(self)
        self.state = Gui.states_dict[target_state_name]
        self.state.apply(self)

    def on_window_closed(self, window_closed_cb):
        self.window.connect('destroy', window_closed_cb)

    def change_settings(self, info):
        if isinstance(info, tuple):
            name, increment = info
        elif isinstance(info, basestring):
            name = info
            increment = None
        else:
            raise TypeError()
        oldval = self.settings[name]
        self.settings.change_value(name, increment)
        newval = self.settings[name]
        if self._settings_change_hoock and oldval != newval:
            self._settings_change_hoock.changed(name, oldval, newval)
        self.state.update_screen(self)

    def commit_changes(self):
        self.settings.save_settings()

    def install_settings_change_hook(self, hoock):
        self._settings_change_hoock = hoock

    def get_settings_value(self, var_name):
        return self.settings[var_name]

    def set_settings_value(self, var_name, value):
        self.settings[var_name] = value

    def get_central_field(self):
        return self._central_field

    def clear_central_field(self):
        self._central_field.foreach(self._central_field.remove)

    def add_central_field(self, widgets_list):
        for (w, pos) in widgets_list:
            new_pos = copy.copy(pos)
            container_loc = self._central_field.get_allocation()
            w_size = w.size_request()
            if new_pos.x() < 0:
                new_pos.set_x(container_loc.width * -new_pos.x() - w_size[0] / 2)
            if new_pos.y() < 0:
                new_pos.set_y(container_loc.height * -new_pos.y() - w_size[1] / 2)
            self._central_field.put(w, int(new_pos.x()), int(new_pos.y()))

    def edit_single_value(self, params):
        editor = SingleValueEditor(*params)
        self.state = editor
        self.state.apply(self)

    def diag_interface(self, params):
        editor = AxisMover(*params)
        self.state = editor
        self.state.apply(self)
        pass

    def edit_double_value(self, params):
        editor = DoubleValueEditor(*params)
        self.state = editor
        self.state.apply(self)

    def __getitem__(self, item):
        m = getattr(self, item)
        if inspect.ismethod(m):
            return m
        else:
            raise KeyError()

    def block_buttons(self, is_blocked):
        for button in buttons:
            self.btn(button).set_blocked(is_blocked)

    def reconnect(self):
        self.emit('reconnect')

    def hold_interface(self, cb):
        self._after_unblock_interface = cb
        self.block_buttons(True)
        self.hold_window.show()

    def _release_interface(self, *args):
        print "Releasing interface"
        self.hold_window.hide()
        self.block_buttons(False)
        if self._after_unblock_interface:
            self._after_unblock_interface()
            self._after_unblock_interface = None

    def machine_start_program(self, program_id, ready_cb):
        print('Starting machine program: {}'.format(program_id))
        self.hold_interface(ready_cb)
        self.emit('process_command_req', program_id, self._release_interface)

    def machine_interrupt_program(self, interrupted_cb):
        self.hold_interface(interrupted_cb)
        self.emit('process_command_req', 'Reset', self._release_interface)

    def on_watch_variable_changed(self, sender, data):
        _id = data['id']
        watcher = self.watchers.get(_id, UnknownWatcher())
        watcher.on_message(data)

    def move_to_nonblock(self, **kwargs):
        print "Move to: {}".format(kwargs)
        self.emit('process_command_req', 'manual_goto', (kwargs, None))

    def home_axis_nonblock(self, axis):
        self.emit('process_command_req', 'Home_axis', (axis, None))

    def output_control_nonblock(self, output_control_dict):
        self.emit('process_command_req', 'manual_output_control', (output_control_dict, None))

    def get_current_pos_by_axis(self, axis):
        axis_map = {
            'X': 0, 'Y': 1, 'Z': 2, 'A': 3, 'B': 4
        }
        return self.watchers[u'WATCH_actual_position'].curent_pos[axis_map[axis]]

    def on_interp_state_got(self, *args, **kwargs):
        self.watchers[u'WATCH_interp_state'].on_got_state(*args, **kwargs)


gobject.type_register(Gui)
