# -*- coding: utf-8 -*-

from twisted.internet import defer

from MachineSequence2 import InitMachineSequence, DisableMachineSequence, StartupSequence, SystemShutdown
from MachineSequence2 import MDI_GO_Sequence, MDI_D_Out_Sequence, WashSequence, MachineReseter, Homer_1_axis
from MachineSequence2 import WorkingSequence


class Controller:
    modes = {
        'MODE_MANUAL': 1,
        'MODE_AUTO': 2,
        'MODE_MDI': 3,
    }

    def __init__(self, factory, settings):
        self.factory = factory

        self.current_mode = Controller.modes['MODE_AUTO']

        self.commands = {
            'Startup': StartupSequence(self),
            'Prepare': InitMachineSequence(self, settings),
            'DisableMachine': DisableMachineSequence(self, settings),
            'SystemShutdown': SystemShutdown(self, lambda: settings.save_settings()),
            'Reset': MachineReseter(self, settings),
            'manual_goto': MDI_GO_Sequence(self),
            'manual_output_control': MDI_D_Out_Sequence(self),
            'Wash': WashSequence(self, settings),
            'Home_axis': Homer_1_axis(self),
            'Work': WorkingSequence(self, settings)
        }

    def process_command(self, _, command, arg):
        print "Command {}({})".format(command, arg)

        d, on_success = self.commands[command].run(arg)
        if on_success:
            d.addCallbacks(on_success, self.error)
        else:
            d.addErrback(self.error)

    def error(self, err):
        print err

    def reconnect(self, *args):
        self.factory.reconnect()

    def send_msg(self, msg):
        return self.factory.send_msg(msg)

    def send_msg_noreply(self, msg):
        return self.factory.send_msg_noreply(msg)

    def get_reactor(self):
        return self.factory.reactor

    def add_wachlist(self, variable):
        return self.factory.add_wachlist(variable)

    def change_mode(self, mode):
        mode_n = Controller.modes[mode]

        def mode_changed(msg):
            if msg[u'code'] == u'?OK':
                self.current_mode = mode_n
            else:
                raise RuntimeError(msg)

        if mode_n != self.current_mode:
            print "Changing mode to {}".format(mode)
            d = self.factory.send_msg({'command': 'put', 'name': 'mode', '0': mode})
            d.addCallback(mode_changed)
            return d
        else:
            return defer.succeed(None)

    def get_current_mode(self, _):
        def _process_mode_ansver(msg):
            self.current_mode = msg[u'data']
            return self.current_mode

        d = self.factory.send_msg({'command': 'get', 'name': 'task_mode'})
        d.addCallback(_process_mode_ansver)
        return d
