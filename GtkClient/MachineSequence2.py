# -*- coding: utf-8 -*-


import StringIO
from abc import ABCMeta, abstractmethod

from twisted.internet import task

from Rasterisers import Y_rasteriser
from common import Point


# -----------------------------------------------------------------------------


class IMachineSequence:
    __metaclass__ = ABCMeta

    @abstractmethod
    def run(self, arg):
        pass


class GCode(IMachineSequence):
    __metaclass__ = ABCMeta

    def __init__(self, controller):
        self.controller = controller

    def set_auto_mode(self, _):
        return self.controller.change_mode('MODE_AUTO')

    def run(self, on_success):
        d = self.set_auto_mode(None)
        d.addCallback(self.generate_GCode)
        d.addCallback(self._load_gcode)
        d.addCallback(self._start_executing)
        return d, on_success

    def _load_gcode(self, g_code):
        return self.controller.send_msg({"command": "put", "name": "program_upload", '0': 'gcode.cnc', '1': g_code})

    def _start_executing(self, _):
        return self.controller.send_msg({"command": "put", "name": "auto", "0": "AUTO_RUN", "1": 0})

    @abstractmethod
    def generate_GCode(self, _):
        pass


# -----------------------------------------------------------------------------


class ConfigRequster:
    def __init__(self, controller, settings=None):
        self.settings = settings
        self.controller = controller
        self.config = None

    def get_config(self, _=None):
        d = self.controller.send_msg({"command": "get", "name": "config"})
        d.addCallback(self._process_config)
        return d

    def _process_config(self, config_json):
        self.config = config_json[u'data'][u'parameters']
        return self.config

    def home_pos(self, _=None):
        homes_nicknames = ('xh', 'yh', 'zh', 'ah', 'bh')
        hp = self.get_config_value(u'TRAJ', u'HOME').split(' ')
        return {homes_nicknames[i]: hp[i] for i in xrange(len(hp))}

    def get_config_value(self, section, key):
        def filter_fun(parameter):
            v = parameter[u'values']
            return v[u'section'] == section and v[u'name'] == key

        res = filter(filter_fun, self.config)
        if res:
            return res[0][u'values'][u'value']

        raise KeyError('No such parameter "[{}]/{}" in configuration'.format(section, key))

    def wash_pos(self, _=None):
        settings_h_values = [self.settings['mode_{}_H'.format(i)] for i in xrange(6)]
        return {'wp_X': self.settings['Washing place X'],
                'wp_Y': self.settings['Washing place Y'],
                'wp_Z': self.settings['Washing place Z'],
                'wp_B': self.get_config_value(u'AXIS_4', u'MAX_LIMIT')}

    def closed_B_pos(self, _=None):
        return float(self.get_config_value(u'AXIS_4', u'CLOSE_POS'))

    def B_hister(self, _=None):
        return float(self.get_config_value(u'AXIS_4', u'H_HISTER'))


class MachineStatusWaiter:
    def __init__(self, controller):
        self.controller = controller

    def wait_machine_stop(self, _=None):
        print "Awaiting machine stops..."

        def schedule_recheck_machine_is_stops(_):
            return task.deferLater(self.controller.get_reactor(), 0, self.wait_machine_stop, None)

        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addErrback(schedule_recheck_machine_is_stops)
        return d

    def _send_req(self, _):
        return self.controller.send_msg({"command": "get", "name": "state"})

    def _check_answer(self, ans):
        if ans[u'data'] == 2:
            raise

    def wait_machine_start(self, _=None):
        print "Awaiting machine starts..."

        def schedule_recheck_machine_moving(_):
            return task.deferLater(self.controller.get_reactor(), 0, self.wait_machine_start, None)

        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addCallbacks(schedule_recheck_machine_moving, lambda _: None)
        return d


class Homer_1_axis(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.stop_waiter = MachineStatusWaiter(controller)

    def run(self, arg):
        _axis, on_ready = arg
        return self.home_axis(_axis), on_ready

    def home_axis(self, _axis='X'):
        def send_home_cmd(_):
            axis2n = {'X': 0, 'Y': 1, 'Z': 2, 'A': 3, 'B': 4}
            if type(_axis) == str:
                axis = axis2n[_axis]
            else:
                axis = _axis
            print "sending home cmd axis {}".format(axis)
            self.controller.send_msg_noreply({"command": "put", "name": "home", "0": str(axis)})

        d = self.controller.change_mode('MODE_MANUAL')
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(send_home_cmd)
        d.addCallback(self.stop_waiter.wait_machine_stop)
        return d


class Homer:
    def __init__(self, controller, settings):
        self.controller = controller
        self.homing_order = None
        self.order_pos = 0
        self.config_requester = ConfigRequster(controller, settings)
        self.stop_waiter = MachineStatusWaiter(controller)

    def machine_home(self, _):
        print "Homing machine..."
        d = self.config_requester.get_config()
        d.addCallback(self._generate_homing_order)
        d.addCallback(self._home_execute)
        return d

    def _generate_homing_order(self, _):
        axes_count = int(self.config_requester.get_config_value(u'TRAJ', u'AXES'))

        homming_sequence = {}
        for axis in xrange(axes_count):
            try:
                homming_sequence[axis] = int(self.config_requester.get_config_value(u'AXIS_{}'.format(axis),
                                                                                    u'HOME_SEQUENCE'))
            except KeyError:
                homming_sequence[axis] = -1

        def sort_home_sequence(seq):
            steps = sorted(seq.values())
            order = {}
            for step in steps:
                order[step] = [key for key, val in seq.iteritems() if val == step]
            return order

        self.homing_order = sort_home_sequence(homming_sequence)
        print "Homing order: {}".format(self.homing_order)
        self.order_pos = 0

    def _home_execute(self, _):
        def send_home_cmd(_):
            for axis in self.homing_order[sorted(self.homing_order.keys())[self.order_pos]]:
                print "Sending home axis {} command".format(axis)
                self.controller.send_msg_noreply({"command": "put", "name": "home", "0": str(axis)})

        def ready(_):
            print "Homing stage {} passed".format(self.order_pos)
            self.order_pos += 1
            if self.order_pos < len(self.homing_order):
                return task.deferLater(self.controller.get_reactor(), 0.5, self._home_execute, None)
            else:
                print "All axes homed!"

        print "Homing pos: {}".format(self.order_pos)
        d = self.controller.change_mode('MODE_MANUAL')
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(send_home_cmd)
        d.addCallback(self.stop_waiter.wait_machine_start)
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(ready)
        return d


class StateSwitcher:
    def __init__(self, controller):
        self.controller = controller

    def switch_machine_state(self, state):
        return self.controller.send_msg({'command': 'put', 'name': 'state', '0': state})


# -----------------------------------------------------------------------------


class StartupSequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller

    def run(self, arg):
        d = self._start_linuxcnc(None)
        d.addCallback(self._machine_wait_start)
        d.addCallback(self.controller.get_current_mode)
        d.addCallback(self._register_watchers)

        return d, arg

    def _start_linuxcnc(self, _):
        print "Starting linuxcnc..."
        return self.controller.send_msg({"command": "put", "name": "startup"})

    def _machine_wait_start(self, _):
        def wait_linuxcnc_rdy(err):
            print "linuxcnc not ready, recheck..."
            return task.deferLater(self.controller.get_reactor(), 0.1, self._machine_wait_start, None)

        def sleep_2s(_):
            return task.deferLater(self.controller.get_reactor(), 2, lambda _: None, None)

        def check_result(msg):
            if msg['data'] != 1:
                raise

        print "Waiting for linuxcnc started..."
        d = self.controller.send_msg({"command": "get", "name": "running"})
        d.addCallback(check_result)
        d.addCallbacks(sleep_2s, wait_linuxcnc_rdy)
        return d

    def _register_watchers(self, _):
        def _register_watches_actual_pos(_):
            return self.controller.add_wachlist('actual_position')

        def _register_watches_pos(_):
            return self.controller.add_wachlist('din')

        def _register_interp_state(_):
            return self.controller.add_wachlist('interp_state')

        d = _register_watches_actual_pos(None)
        d.addCallback(_register_watches_pos)
        d.addCallback(_register_interp_state)
        return d


class InitMachineSequence(IMachineSequence):
    def __init__(self, controller, settings):
        self.state_switcher = StateSwitcher(controller)
        self.controller = controller
        self.homer = Homer(controller, settings)
        self.reseter = MachineReseter(controller, settings)
        self.machine_waiter = MachineStatusWaiter(controller)

    def run(self, arg):
        d = self._machine_reset_estop(None)
        d.addCallback(self.homer.config_requester.get_config)
        d.addCallback(self._check_limits)
        d.addCallback(self._machine_enable)
        d.addCallback(self.homer.machine_home)
        d.addCallback(self.machine_waiter.wait_machine_stop)
        d.addCallback(self.reseter.reset_machine)
        return d, arg

    def _machine_reset_estop(self, _):
        print "Resetting ESTOP..."
        return self.state_switcher.switch_machine_state('STATE_ESTOP_RESET')

    def _machine_enable(self, _):
        print "Turning machine ON..."
        return self.state_switcher.switch_machine_state('STATE_ON')

    def _check_limits(self, _):
        config_requester = self.homer.config_requester
        settings = self.homer.config_requester.settings
        axes_count = int(config_requester.get_config_value(u'TRAJ', u'AXES'))
        axes = ('X', 'Y', 'H', 'A', 'Q')

        minimums = {
            axes[axis]: float(config_requester.get_config_value(u'AXIS_{}'.format(axis), u'MIN_LIMIT'))
            for axis in xrange(axes_count)}
        maximums = {
            axes[axis]: float(config_requester.get_config_value(u'AXIS_{}'.format(axis), u'MAX_LIMIT'))
            for axis in xrange(axes_count)}

        settings.set_axis_min_limit(**minimums)
        settings.set_axis_max_limit(**maximums)
        settings.save_settings()


class DisableMachineSequence(IMachineSequence):
    def __init__(self, controller, settings):
        self.state_switcher = StateSwitcher(controller)
        self.reseter = MachineReseter(controller, settings)

    def run(self, arg):
        d, _ = self.reseter.run(None)
        d.addCallback(self._machine_disable)
        return d, arg

    def _machine_disable(self, _):
        return self.state_switcher.switch_machine_state('STATE_ESTOP')


class SystemShutdown(IMachineSequence):
    def __init__(self, controller, before):
        self.controller = controller
        self.beforecb = before

    def run(self, arg):
        d = self._shutdown_linuxcnc(None)
        d.addCallback(self._shutdown_linuxcnc)
        return d, arg

    def _shutdown_linuxcnc(self, _):
        def shadule_poweroff(_):
            task.deferLater(self.controller.get_reactor(), 10, self._system_shutdown, None)

        d = self.controller.send_msg({"command": "put", "name": "shutdown"})
        d.addCallback(shadule_poweroff)
        return d

    def _system_shutdown(self, _):
        import os
        self.beforecb()
        os.system('sync && sudo poweroff')
        self.controller.get_reactor().stop()
        print "Powering machine off"


class MDI_GO_Sequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.axis_dict = None

    def run(self, arg):
        self.axis_dict, on_success = arg
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        return d, on_success

    def _process_mdi(self, _):
        mdi_command = "G0"
        for key, val in self.axis_dict.iteritems():
            mdi_command += " {}{}".format(key, val)
        return self.controller.send_msg_noreply({"command": "put", "name": "mdi", '0': mdi_command})


class MDI_D_Out_Sequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.out_dict = None

    def run(self, arg):
        self.out_dict, on_success = arg
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        return d, on_success

    def _process_mdi(self, _):
        for output, newstate in self.out_dict.iteritems():
            mdi_command = '{} P{}'.format('M64' if newstate else 'M65', output)
            self.controller.send_msg_noreply({"command": "put", "name": "mdi", '0': mdi_command})


# -----------------------------------------------------------------------------


class MachineReseter(GCode):
    def __init__(self, controller, settings):
        GCode.__init__(self, controller)
        self.config_getter = ConfigRequster(controller, settings)

    def run(self, on_success):
        return self.reset_machine(None), on_success

    def generate_GCode(self, _):
        d = self.config_getter.get_config()
        d.addCallback(self.generate_goto_home)
        return d

    def reset_machine(self, _):
        d = self.send_abort(None)
        d.addCallback(self.set_auto_mode)
        d.addCallback(self.generate_GCode)
        d.addCallback(self._load_gcode)
        d.addCallback(self._start_executing)
        return d

    def send_abort(self, _):
        return self.controller.send_msg({"command": "put", "name": "abort"})

    def generate_goto_home(self, _):
        return """%
G0 B{cb_hp}  
G0 B{cb}          
M65 P0
G0 X{xh}Y{yh}Z{zh}A{ah}
M30
%""".format(cb_hp=self.config_getter.closed_B_pos() + self.config_getter.B_hister(),
            cb=self.config_getter.closed_B_pos(), **self.config_getter.home_pos(None))


class WashSequence(GCode):
    def __init__(self, controller, settings):
        GCode.__init__(self, controller)
        self.config_getter = ConfigRequster(controller, settings)

    def generate_GCode(self, _):
        d = self.config_getter.get_config()
        d.addCallback(self.generate_goto_wp)
        return d

    def generate_goto_wp(self, _):
        return """%
G0 X{wp_X} Y{wp_Y} Z{wp_Z}
M64 P0
G0 B{wp_B}
M30
%""".format(**self.config_getter.wash_pos())


class WorkingSequence(GCode):
    def __init__(self, controller, settings, rasteriser=Y_rasteriser()):
        super(WorkingSequence, self).__init__(controller)
        self.config_getter = ConfigRequster(controller, settings)
        self.rasteriser = rasteriser

    def generate_GCode(self, _):
        d = self.config_getter.get_config()
        d.addCallback(self.generate_programm)
        return d

    def generate_programm(self, _):
        code = StringIO.StringIO()
        settings = self.config_getter.settings
        mode = settings['Work Mode']
        work_rect_start = Point(settings['mode_{}_X0'.format(mode)], settings['mode_{}_Y0'.format(mode)])
        work_rect_end = Point(settings['mode_{}_X'.format(mode)], settings['mode_{}_Y'.format(mode)])
        working_H = settings['mode_{}_H'.format(mode)]
        working_Q = settings['mode_{}_Q'.format(mode)]
        raster_step = settings['mode_{}_T'.format(mode)]
        closed_Q = self.config_getter.closed_B_pos()
        wash_pos = self.config_getter.wash_pos()
        #B_hister = self.config_getter.B_hister()

        def _close_B():
            #            return """
            #G0 B{pc}
            #G0 B{closed}\n""".format(pc=closed_Q + B_hister, closed=closed_Q)
            return "G0 B{closed}\n".format(closed=closed_Q)

        raster, new_start, new_end, finish = self.rasteriser.generate_raster(work_rect_start, work_rect_end,
                                                                             raster_step)

        code.write("%\n")

        swt = float(settings['Startup washing time'])
        if swt > 0.01:
            # G0 to wash place, A = 0
            code.write("G0 X{wp_X} Y{wp_Y} Z{wp_Z} A0 B{b_closed}".format(b_closed=closed_Q, **wash_pos))

            # open clapan + B to wash place
            code.write("""
M64 P0
G0 B{wp_B}\n""".format(**wash_pos))

            # delay prestart wash
            code.write("G4 P{}\n".format(swt))

            # B to work position
            code.write("G0 B{}\n".format(working_Q))

            # G0 start position
            code.write("G0 X{start_x} Y{start_y} Z{z}\n".format(start_x=new_start.x, start_y=new_start.y, z=working_H))
        else:
            # G0 to wash place
            code.write("G0 X{wp_X} Y{wp_Y} Z{wp_Z} B{b_closed} A0\n".format(b_closed=closed_Q, **wash_pos))

            # open clapan
            code.write("M64 P0\n")

            # G0 start position + open clapan
            #            code.write("""
            #G0 B{wQp}         
            #G0 B{wQ}
            #G0 X{start_x} Y{start_y} Z{z}\n""".format(wQp=working_Q + B_hister, start_x=new_start.x, start_y=new_start.y,
            #                                          z=working_H, wQ=working_Q))
            code.write("""     
G0 B{wQ}
G0 X{start_x} Y{start_y} Z{z}\n""".format(start_x=new_start.x, start_y=new_start.y, z=working_H, wQ=working_Q))

        # set F
        code.write("G1 F{}\n".format(settings['G1 speed']))

        # generate raster
        code.write(raster)

        if mode == 0 or mode == 1:
            cwt = float(settings['mode_{}_cwt'.format(mode)])
            if cwt > 0.01:
                # B to closed
                code.write(_close_B())

                # G0 to wash place
                code.write("G0 X{wp_X} Y{wp_Y} Z{wp_Z}\n".format(**wash_pos))

                # B to wash place, A=180
                code.write("G0 B{wp_B} A180\n".format(**wash_pos))

                # delay wash
                code.write("G4 P{}\n".format(cwt))

                # B to work position
                code.write("G0 B{}\n".format(working_Q))

                # G0 start position
                code.write("G0 X{start_x} Y{start_y} Z{z}\n".format(start_x=new_start.x, start_y=new_start.y,
                                                                    z=working_H))

                # generate raster
                code.write(raster)
            else:
                # G0 A180
                code.write("G0 A180\n")

                # goto end pos
                code.write("G1 X{} Y{}\n".format(finish.x, finish.y))

                # inverted raster
                raster, _, _, _ = self.rasteriser.generate_reversed_raster(work_rect_start, work_rect_end, raster_step)
                code.write(raster)

        # G0 to wash place
        code.write("G0 X{wp_X} Y{wp_Y} Z{wp_Z}\n".format(**wash_pos))

        # B to closed
        code.write(_close_B())

        # klapan off
        code.write("M65 P0\n")

        # G0 zero
        code.write("G0 X{xh}Y{yh}Z{zh}A{ah}".format(**self.config_getter.home_pos()))

        code.write("""
M30
%""")
        return code.getvalue()
