# -*- coding: utf-8 -*-

import gobject
import json

from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory
from twisted.internet import defer


class StatusUpdater(gobject.GObject):
    __gsignals__ = {
        'logged_in': (gobject.SIGNAL_RUN_LAST,  # default
                      gobject.TYPE_NONE,  # return value
                      (gobject.TYPE_BOOLEAN,)  # parameters types
                      ),
        'watch_got': (gobject.SIGNAL_RUN_LAST,  # default
                      gobject.TYPE_NONE,  # return value
                      (gobject.TYPE_PYOBJECT,)  # parameters types
                      ),
    }

    def emit_signal(self, signame, *args):
        self.emit(signame, *args)


gobject.type_register(StatusUpdater)


class MyWSClientProto(WebSocketClientProtocol):
    def onConnect(self, response):
        print("Connected to Rockhopper server: {0}".format(response.peer))

    def onOpen(self):
        print("WebSocket connection open.")
        self.factory.client_ready(self)

    def onMessage(self, payload, isBinary):
        self.factory.got_message(payload)

    def onClose(self, wasClean, code, reason):
        print("Rockhopper connection closed {0}".format('clean' if wasClean else 'unexpected'))


class MayWSClientFactory(WebSocketClientFactory):
    protocol = MyWSClientProto
    login = 'default'
    password = 'default'

    sync_cmd_id = 'Sync-cmd'

    def __init__(self, adr, port=80, **kwargs):
        if 'debug' in kwargs:
            self.debug = kwargs['debug']
            kwargs.pop('debug', None)
        else:
            self.debug = False

        self.adr = adr
        self.port = port
        ws_url = u"ws://{}:{}/websocket/linuxcnc".format(adr, port)
        WebSocketClientFactory.__init__(self, ws_url, **kwargs)
        self.client = None
        self.sync_defer = None

        self.emitter = StatusUpdater()

        self.watchlist = []

    def reconnect(self):
        print "Connecting to Rockhopper server..."
        if self.client:
            c, self.client = self.client, None
            c.sendClose()

        self.reactor.connectTCP(self.adr, self.port, self)

    def client_ready(self, client):
        self.client = client

        def emit_logged_in(_):
            self.emitter.emit_signal('logged_in', True)

        self.send_msg(
            {"user": MayWSClientFactory.login,
             "password": MayWSClientFactory.password}).addCallback(emit_logged_in)

    def got_message(self, message):
        message = json.loads(message, encoding='utf-8')
        if message['id'] == MayWSClientFactory.sync_cmd_id and self.sync_defer:
            if self.debug:
                print "RSV: ", message
            d, self.sync_defer = self.sync_defer, None
            d.callback(message)
        elif message['id'] in self.watchlist:
            self.emitter.emit_signal('watch_got', message)

    def send_msg(self, msg):
        if self.sync_defer:
            raise RuntimeError('send_msg(): Synchronous operation in progress! DATA: {}'.format(msg))
        elif self.client:
            msg['id'] = MayWSClientFactory.sync_cmd_id
            if self.debug:
                print "SND: ", msg
            self.sync_defer = defer.Deferred()
            self.client.sendMessage(json.dumps(msg, encoding='utf-8'))
            return self.sync_defer
        else:
            raise RuntimeError('Client not connected!')

    def send_msg_noreply(self, msg):
        if self.sync_defer:
            raise RuntimeError('send_msg_noreply(): Synchronous operation in progress! DATA: {}'.format(msg))
        elif self.client:
            if self.debug:
                print "SND: ", msg
            self.client.sendMessage(json.dumps(msg))
        else:
            raise RuntimeError('Client not connected!')

    def add_wachlist(self, value_name):
        _id = 'WATCH_{}'.format(value_name)
        self.watchlist.append(_id)
        self.send_msg_noreply({"command": "watch", "name": value_name, 'id': _id})
        return defer.succeed(None)
