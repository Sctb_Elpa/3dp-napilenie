# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod

from ControlBtn import *


class IHalPinMapper:
    """ интерфейс маппера button -> HALPinName """

    __metaclass__ = ABCMeta

    @abstractmethod
    def GetHalPin(self, buttonname):
        """ Получить hal-pin кнопки, которая будет физическим аналогом buttonName """

    def getButton(self, buttonname):
        """ Получить дискриптор кнопки интерфейса """


class IInterfaceState:
    """ Интерфейс состояния окна """

    __metaclass__ = ABCMeta

    @abstractmethod
    def apply_state(self, controller):
        """ Применить состояние к окну """


class DisabledScreen(IInterfaceState):
    """ Начальный экран """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, None)
        controller.ButtonSetText(InterfaceController.button9, u'Включить')
        controller.ButtonSetText(InterfaceController.button4, None)
        controller.ButtonSetText(InterfaceController.button6, None)
        controller.ButtonSetText(InterfaceController.button1, None)
        controller.ButtonSetText(InterfaceController.button3, None)


class HomeScreen(IInterfaceState):
    """ Домашинй экран """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, u'Выключить')
        controller.ButtonSetText(InterfaceController.button9, u'Старт')
        controller.ButtonSetText(InterfaceController.button4, u'Расход')
        controller.ButtonSetText(InterfaceController.button6, u'Режим')
        controller.ButtonSetText(InterfaceController.button1, u'Дополнительно')
        controller.ButtonSetText(InterfaceController.button3, u'Промывка')


class Exec(IInterfaceState):
    """ Экран работы """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, u'Стоп')
        controller.ButtonSetText(InterfaceController.button9, None)
        controller.ButtonSetText(InterfaceController.button4, None)
        controller.ButtonSetText(InterfaceController.button6, None)
        controller.ButtonSetText(InterfaceController.button1, None)
        controller.ButtonSetText(InterfaceController.button3, None)


class Washing(IInterfaceState):
    """ Экран промывки """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, u'Стоп')
        controller.ButtonSetText(InterfaceController.button9, None)
        controller.ButtonSetText(InterfaceController.button4, None)
        controller.ButtonSetText(InterfaceController.button6, None)
        controller.ButtonSetText(InterfaceController.button1, None)
        controller.ButtonSetText(InterfaceController.button3, None)


class EditConsumption(IInterfaceState):
    """ Экран редактироваия расхода """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, None)
        controller.ButtonSetText(InterfaceController.button9, u'OK')
        controller.ButtonSetText(InterfaceController.button4, u'-')
        controller.ButtonSetText(InterfaceController.button6, u'+')
        controller.ButtonSetText(InterfaceController.button1, None)
        controller.ButtonSetText(InterfaceController.button3, None)


class OptionsAndDiadnostic(IInterfaceState):
    """ Экран настроек и диагностики """

    def apply_state(self, controller):
        controller.ButtonSetText(InterfaceController.button7, None)
        controller.ButtonSetText(InterfaceController.button9, None)
        controller.ButtonSetText(InterfaceController.button4, None)
        controller.ButtonSetText(InterfaceController.button6, None)
        controller.ButtonSetText(InterfaceController.button1, None)
        controller.ButtonSetText(InterfaceController.button3, None)


class InterfaceController:
    button7 = 'Upper-left button'
    button4 = 'Mid-left button'
    button1 = 'Down-left button'
    button9 = 'Upper-right button'
    button6 = 'Mid-right button'
    button3 = 'Down-right button'

    buttons = (button7, button9, button4, button6, button1, button3)

    def __init__(self, windowbuilder, halcomp, pinmapper, accelerators):
        self.halcomp = halcomp
        self.windowbuilder = windowbuilder

        self.btns = {}
        for key in InterfaceController.buttons:
            value = pinmapper.GetHalPin(key)
            btn = pinmapper.getButton(key)
            cbtn = ControlBtn(btn, value, halcomp)
            # self.add_accelerator(btn, pinmapper.getHotkey(key), accelerators)
            self.btns[key] = cbtn

        self.DisabledScreen = DisabledScreen()
        self.Homescreen = HomeScreen()
        #self.Reset()


def Reset(self):
    """Reset machine interface state"""
    self.SwitchState(self.Homescreen)


def ButtonSetText(self, btn_name, text):
    """Set interface button text"""
    self.btns[btn_name].set_btn_text(text)
    print 'Setting label "{}" for {}'.format(text, btn_name)


def SetButtonCallback(self, btnname, callback, cookie):
    pass


def SwitchState(self, newState):
    """Change window state"""
    self.CurrentState = newState
    self.CurrentState.apply_state(self)


def add_accelerator(self, obj, accelerator, accelerators):
    """Adds a keyboard shortcut"""
    if accelerator is not None:
        key, mod = gtk.accelerator_parse(accelerator)
        obj.add_accelerator('clicked', accelerators, key, mod, gtk.ACCEL_VISIBLE)
