#!/usr/bin/env python

import argparse

from controller import Controller
from gui import Gui
from remote import MayWSClientFactory
from settings import Settings


def main():
    from twisted.internet import gtk2reactor
    gtk2reactor.install()
    from twisted.internet import reactor

    parser = argparse.ArgumentParser()
    parser.add_argument('--remote', type=str, default=None)

    args = parser.parse_args()
    if not args.remote:
        print('Starting local GIU')
        adr = 'localhost'
        port = 8000
    else:
        print('Starting remote GUI for {}'.format(args.remote))
        if ':' not in args.remote:
            port = 8000
            adr = args.remote
        else:
            adr, port = args.remote.split(':')
            port = int(port)

    factory = MayWSClientFactory(adr, port, protocols=['linuxcnc'])
    # factory.debug = True

    settings = Settings()

    _gui = Gui(settings)
    controller = Controller(factory, settings)

    factory.emitter.connect('logged_in', _gui.connected)

    _gui.connect('reconnect', controller.reconnect)
    _gui.connect('process_command_req', controller.process_command)
    factory.emitter.connect('watch_got', _gui.on_watch_variable_changed)

    _gui.reset()
    _gui.show()
    _gui.on_window_closed(window_closed)
    reactor.run()

    settings.save_settings()


def window_closed(sender):
    from twisted.internet import reactor
    reactor.stop()


if __name__ == "__main__":
    main()
