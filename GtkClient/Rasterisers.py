# -*- coding: utf-8 -*-

import StringIO
from math import ceil

from common import Point, Rect


class Y_rasteriser:
    def __init__(self):
        self.curent_pos = Point()

    def generate_raster(self, start_pos, end_pos, lines_offset):
        code = StringIO.StringIO()

        start_pos = start_pos.copy()
        end_pos = end_pos.copy()

        x_size = abs(abs(start_pos.x) - abs(end_pos.x))
        lines_count = int(ceil(x_size / float(lines_offset)))  # + 1
        print "Rasterising From {} to {} by {} Y-lines".format(start_pos, end_pos, lines_count)
        new_x_size = (lines_count - 1) * float(lines_offset)
        _overflow = (new_x_size - x_size) / 2
        if start_pos.x < end_pos.x:
            start_pos.x -= _overflow
            end_pos.x += _overflow
        else:
            start_pos.x += _overflow
            end_pos.x -= _overflow

        self.curent_pos = start_pos.copy()
        print "New limits {} -> {} with overflow={}".format(start_pos, end_pos, _overflow)
        if start_pos.x > end_pos.x:
            lines_offset *= -1  # обратный ход по X

        code.write("G1 X{} Y{} (start)\n".format(start_pos.x, start_pos.y))

        yp = 0
        while True:
            _from_y = self.curent_pos.y

            code.write(self.gen_y_line(start_pos.y, end_pos.y))
            lines_count -= 1

            if lines_count == 0:
                break

            yp = self.curent_pos.y < _from_y
            xp = lines_offset > 0

            ark_clockwise = not ((yp and xp) or (not yp and not xp))

            code.write(self.gen_line_change(lines_offset, ark_clockwise))

        tail = lines_offset / 2.0

        code.write("G1 Y{}\n".format(self.curent_pos.y + (tail if yp else -tail)))

        return code.getvalue(), start_pos, end_pos, self.curent_pos

    def gen_y_line(self, ys, ye):
        if ys < ye:
            dest = ye if self.curent_pos.y <= ys else ys
        else:
            dest = ye if self.curent_pos.y >= ys else ys
        self.curent_pos.y = dest
        return "G1 Y{}\n".format(dest)

    def gen_line_change(self, x_shift, ark_clockwise):
        self.curent_pos.x += x_shift
        code = 'G2' if ark_clockwise else 'G3'
        return "{code} X{xd} I{i}\n".format(code=code, xd=self.curent_pos.x, i=x_shift / 2.0)

    def generate_reversed_raster(self, start_pos, end_pos, lines_offset):
        _, start_pos, end_pos, fin = self.generate_raster(start_pos, end_pos, lines_offset)

        code = StringIO.StringIO()

        code.write("G1 X{} Y{} (start reversed)\n".format(fin.x, fin.y))

        workzone = Rect(start_pos, end_pos)
        new_end = workzone.oposit_point(fin)

        return self.generate_raster(fin, new_end, lines_offset)



def _test_rasteriser():
    r = Y_rasteriser()
    start = Point(15.3, 18)
    end = Point(105, 97.3)
    return r.generate_raster(end, start, 8)
