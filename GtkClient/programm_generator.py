# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod
from cStringIO import StringIO

gcode_programs = ('Startup', 'Wash')

"""
Приняты следующие умолчания
1. Ось Х с лева на право, положение 0 - слева
2. Ось Y из глубины вперёд, положение 0 - сзади
3. Ось Z снизу в верх, положение 0 - сверху
4. Ось A - полярная, положение 0 - пластины горизонтально
5. Ось B - фэрограф, положение 0 - закрыт до упора
6. Клапан по управляется через комманду M64 P0 / М65 P0 => motion.digital-out-00
"""


class IGCodeGenerator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def generate(self, settings):
        """ Генерирует программу определенного типа из настроек """

    @staticmethod
    def generate_program(program_id, settings):
        pass

    def __init__(self):
        self._buf = StringIO()
        self.put_code('%')

    def put_code(self, code):
        self._buf.write(code + '\n\r')

    def _get_result(self):
        return self._buf.getvalue()

    def _finalise_program(self):
        self.put_code('%')

    def _init_program(self):
        self.put_code('%')


class StartupCodeGenerator(IGCodeGenerator):
    def __init__(self):
        IGCodeGenerator.__init__(self)

    def generate(self, settings):
        self._init_program()
        # 1. едем к чашке
        self.put_code('G0 X{} Y{} Z{} A45 B0'.format(settings['Washing place X'],
                                                     settings['Washing place Y'],
                                                     settings['mode_0_H']))
        # 2. Включаем клапан
        self.put_code('M64 P0')
        # 3. Пауза 1 с
        self.put_code('G4 P1.0')
        # 4. Выключить клапан
        self.put_code('M65 P0')
        self._finalise_program()
        return self._get_result()
