# -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod
from collections import OrderedDict


class IWatcher:
    __metaclass__ = ABCMeta

    @abstractmethod
    def on_message(self, message):
        """
        :param message: watch message
        """


class UnknownWatcher(IWatcher):
    def on_message(self, message):
        print 'Unknown wach message type: {}'.format(message['id'])


class PositionWatcher(IWatcher):
    def __init__(self, status_bar):
        self._status_bar = status_bar
        self._status_bar.set_use_markup(True)
        self.curent_pos = [0.0, 0.0, 0.0, 0.0, 0.0]
        self.update_status_bar()

    def on_message(self, message):
        if message[u'code'] == u'?OK':
            self.curent_pos = message[u'data'][0:5]
            self.update_status_bar()

    def update_status_bar(self):
        self._status_bar.set_markup(u"\
<span foreground='#f44336'>X: </span><span foreground='#607d8b' size='large'>{:.2f}</span>\
\t\t<span foreground='#e91e63'>Y: </span><span foreground='#607d8b' size='large'>{:.2f}</span>\
\t\t<span foreground='#9c27b0'>Z: </span><span foreground='#607d8b' size='large'>{:.2f}</span>\
\t\t<span foreground='#673ab7'>R: </span><span foreground='#607d8b' size='large'>{:.2f}</span>\
\t\t<span foreground='#3f51b5'>Q: </span><span foreground='#607d8b' size='large'>{:.2f}</span>".format(
            *self.curent_pos))


class HWButtonWacher(IWatcher):
    def __init__(self, buttons, _map):
        """
        :param buttons:
        :param _map: словарь вида {"button_name": pin_number, ...}
        """
        self.buttons = buttons
        self._map = _map
        self.current_btn_state = dict.fromkeys(_map.keys(), 0)
        self.prev_homes_state = [False, False, False, False, False]

    def on_message(self, message):
        if message[u'code'] == u'?OK':
            new_btn_state = dict.fromkeys(self._map.keys(), 0)
            data = message[u'data']
            for key, value in self._map.iteritems():
                new_btn_state[key] = data[value]

            changed = self.find_first_change(new_btn_state)
            if changed:
                newstate = new_btn_state[changed]
                self.buttons[changed].changed(None, newstate)
                self.current_btn_state = new_btn_state

            self.print_homes_state(data)

    def print_homes_state(self, d):
        hs = d[10:15]
        if hs != self.prev_homes_state:
            self.prev_homes_state = hs
            print 'New homes state: {}'.format(hs)

    def find_first_change(self, newstate):
        for key, value in self.current_btn_state.iteritems():
            if newstate[key] != value:
                return key

        return None


class InterpretatorState(IWatcher):
    def __init__(self):
        self.interp_state = "EMC_TASK_INTERP_IDLE"
        self.on_interp_state = OrderedDict()
        self.on_interp_state["EMC_TASK_INTERP_IDLE"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_READING"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_PAUSED"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_WAITING"] = (None, None)

    def on_message(self, message):
        if message[u'data'] == 'Server is not running.':
            self.interp_state = "EMC_TASK_INTERP_IDLE"
        else:
            self.interp_state = self.on_interp_state.keys()[int(message[u'data']) - 1]
        print "Interp state changed to {}".format(self.interp_state)

        callback, cookie = self.on_interp_state[self.interp_state]

        if callback:
            callback(cookie)

    def on_got_state(self, state, callback, cookie=None):
        self.on_interp_state[state] = (callback, cookie)
