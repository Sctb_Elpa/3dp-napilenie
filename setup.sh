#!/bin/bash

dtbo_err () {
	echo "Error loading device tree overlay file: $DTBO" >&2
	exit 1
}

pin_err () {
	echo "Error exporting pin:$PIN" >&2
	exit 1
}

dir_err () {
	echo "Error setting direction:$DIR on pin:$PIN" >&2
	exit 1
}

SLOTS=/sys/devices/bone_capemgr.*/slots

# Make sure required device tree overlay(s) are loaded
for DTBO in BB-3DP-NAPILENIE ; do

	if grep -q $DTBO $SLOTS ; then
		echo $DTBO overlay found
	else
		echo Loading $DTBO overlay
		sudo -A su -c "echo $DTBO > $SLOTS" || dtbo_err
		sleep 1
	fi
done;

if [ ! -r /sys/class/uio/uio0 ] ; then
	echo PRU control files not found in /sys/class/uio/uio0 >&2
	exit 1;
fi

# Export GPIO pins
# This really only needs to be done to enable the low-level clocks for the GPIO
# modules.  There is probably a better way to do this...
while read PIN DIR JUNK ; do
        case "$PIN" in
        ""|\#*)	
		continue ;;
        *)
		[ -r /sys/class/gpio/gpio$PIN ] && continue
                sudo -A su -c "echo $PIN > /sys/class/gpio/export" || pin_err
		sudo -A su -c "echo $DIR > /sys/class/gpio/gpio$PIN/direction" || dir_err
                ;;
        esac

done <<- EOF
    #buttons
	14	in	# B1
	111 in  # B3
	112 in  # B4
	110 in  # B5
	23  in  # B7
	47  in  # B8

    68  out # enable
    22  out # Klapan

	69  out # STEP 1
	44  out # STEP 2
	45  out # STEP 3
	26  out # STEP 4
	66  out # STEP 5

	15  out # DIR 1
	49  out # DIR 2
	2   out # DIR 3
	3   out # DIR 4
	67  out # DIR 5

	# INPUTS
	51  in  # IN 1
	48  in  # IN 2
	31  in  # IN 3
	50  in  # IN 4
	30  in  # IN 5
EOF
