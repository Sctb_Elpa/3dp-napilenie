#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser
import StringIO
import argparse
import hashlib
import os.path
import subprocess
import sys

import cpuinfo

from Rockhopper.LinuxCNCWebSktSvr import main_loop as rh_main_loop
from Rockhopper_mod import my_rh_main

config_template_filename = '3dp-napilenie.templae.ini'
result_config_filename = '3dp-napilenie.work.ini'

hal_template_filename = '3dp-napilenie.template.hal'
hal_config_filename = '3dp-napilenie.work.hal'

sim_buttons_panel = './sim_buttons_panel.py'


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def pin_unpack(pin):
    return pin / 100, pin % 100


def generate_pru_config(axis_n, pru_name='hpg'):
    """
    This code generates config for pru
    """

    return """
# ################
# Axis {axis_n}
# ################

# axis enable chain
newsig emcmot.0{axis_n}.enable bit
sets emcmot.0{axis_n}.enable FALSE

net emcmot.0{axis_n}.enable <= axis.{axis_n}.amp-enable-out
net emcmot.0{axis_n}.enable => hpg.stepgen.0{axis_n}.enable
#net emcmot.0{axis_n}.enable => hpg.pwmgen.0{axis_n}.out.0{axis_n}.enable

# position command and feedback
net emcmot.0{axis_n}.pos-cmd <= axis.{axis_n}.motor-pos-cmd
net emcmot.0{axis_n}.pos-cmd => hpg.stepgen.0{axis_n}.position-cmd

net motor.0{axis_n}.pos-fb <= hpg.stepgen.0{axis_n}.position-fb
net motor.0{axis_n}.pos-fb => axis.{axis_n}.motor-pos-fb

# timing parameters
setp {pru}.stepgen.0{axis_n}.dirsetup        [AXIS_{axis_n}]DIRSETUP
setp {pru}.stepgen.0{axis_n}.dirhold         [AXIS_{axis_n}]DIRHOLD

setp {pru}.stepgen.0{axis_n}.steplen         [AXIS_{axis_n}]STEPLEN
setp {pru}.stepgen.0{axis_n}.stepspace       [AXIS_{axis_n}]STEPSPACE

setp {pru}.stepgen.0{axis_n}.position-scale  [AXIS_{axis_n}]SCALE

setp {pru}.stepgen.0{axis_n}.maxvel          [AXIS_{axis_n}]STEPGEN_MAX_VEL
setp {pru}.stepgen.0{axis_n}.maxaccel        [AXIS_{axis_n}]STEPGEN_MAX_ACC

setp {pru}.stepgen.0{axis_n}.steppin         [AXIS_{axis_n}]STEP_PIN
setp {pru}.stepgen.0{axis_n}.dirpin          [AXIS_{axis_n}]DIR_PIN
""".format(axis_n=axis_n, pru=pru_name)


def get_buttons_map(config):
    buttons_map = {}
    for i in xrange(10):
        key = 'B{}'.format(i)
        try:
            v = int(config.get('PINS', key))
            buttons_map[key] = v
        except ConfigParser.NoOptionError:
            pass

    return buttons_map


def revers_interface_buttons(bmap):
    result = {}
    for i in xrange(10):
        key_orig = 'B{}'.format(i)
        key_new = 'B{}'.format(10 - i)
        try:
            result[key_new] = bmap[key_orig]
        except KeyError:
            pass
    return result


def get_home_map(config):
    homes = {}
    for axis in xrange(int(config.get('TRAJ', 'AXES'))):
        key = 'H{}'.format(axis)
        try:
            pin = int(config.get('PINS', key))
            is_inverted = config.get('PINS', 'H{}_INVERTED'.format(axis)) or False
            homes[key] = (pin, is_inverted)
        except ConfigParser.NoOptionError:
            pass

    return homes


class TextTemplate:
    def __init__(self, text):
        self.text = text

    def replace(self, placeholder, text):
        self.text = self.text.replace('#%%{}%%'.format(placeholder), text)
        return self.text

    def append(self, text):
        self.text += text

    def get(self):
        return self.text


class RawMultiConfigParser(ConfigParser.RawConfigParser):
    """
    Write lists as multy-key values
    Exsample:
        ini.set('SECTION', 'key', ['val1', 'val2'])

        [SECTION]
        key = val1
        key = val2
    """

    def write(self, fp):
        """Write an .ini-format representation of the configuration state."""
        if self._defaults:
            fp.write("[%s]\n" % ConfigParser.DEFAULTSECT)
            for (key, value) in self._defaults.items():
                fp.write("%s = %s\n" % (key, str(value).replace('\n', '\n\t')))
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key == "__name__":
                    continue
                if (value is not None) or (self._optcre == self.OPTCRE):
                    if isinstance(value, list):
                        res = []
                        for val in value:
                            res.append(" = ".join((key, str(val).replace('\n', '\n\t'))))
                        key = '\n'.join(res)
                    else:
                        key = " = ".join((key, str(value).replace('\n', '\n\t')))
                fp.write("%s\n" % (key))
            fp.write("\n")


def generate_hal(is_sim, config):
    # hal
    with open(hal_template_filename, 'r') as hal_template:
        hal_data = TextTemplate(hal_template.read())

    # modify template
    if is_sim:
        hal_data.append("""
# virtual buttons panel
loadusr -Wn sim_buttons {}

net b7 sim_buttons.b7 => motion.digital-in-07
net b9 sim_buttons.b9 => motion.digital-in-09
net b4 sim_buttons.b4 => motion.digital-in-04
net b6 sim_buttons.b6 => motion.digital-in-06
net b1 sim_buttons.b1 => motion.digital-in-01
net b3 sim_buttons.b3 => motion.digital-in-03
        """.format(sim_buttons_panel))
    else:
        buttons_map = get_buttons_map(config)

        if config.get('PINS', 'IS_DISPLAY_INVERTED') == 'TRUE':
            buttons_map = revers_interface_buttons(buttons_map)

        homemap = get_home_map(config)

        axes_count = int(config.get('TRAJ', 'AXES'))
        enable_pin = int(config.get('PINS', 'ENABLE_PIN'))
        klapan_pin = int(config.get('PINS', 'KLAPAN_PIN'))
        pru_name = config.get('PRUCONF', 'PRU_NICKNAME')

        _home_pins = [pin for key, (pin, _) in homemap.iteritems()]

        def semicolon_separeted_numbers(l):
            sl = map(str, l)
            return ','.join(sl)

        hal_data.replace('setup', 'loadusr -w ./setup.sh')

        hal_data.replace('loadrt', """
loadrt trivkins
loadrt tp
loadrt [EMCMOT]EMCMOT servo_period_nsec=[EMCMOT]SERVO_PERIOD num_joints=[TRAJ]AXES tp=tp kins=trivkins num_dio=[EMCMOT]DIGITAL_IO
loadrt [PRUCONF](DRIVER) prucode=$(HAL_RTMOD_DIR)/[PRUCONF](PRUBIN) [PRUCONF](CONFIG) halname=[PRUCONF]PRU_NICKNAME""")

        hal_data.replace('bbb_pins', """
loadrt hal_bb_gpio output_pins={outputs} input_pins={inputs}
""".format(outputs=semicolon_separeted_numbers((enable_pin, klapan_pin)),
           inputs=semicolon_separeted_numbers(buttons_map.values() + _home_pins)))

        hal_data.replace('threads', """
addf {pru_name}.capture-position          servo-thread
addf motion-command-handler               servo-thread
addf motion-controller                    servo-thread
addf {pru_name}.update                    servo-thread
addf bb_gpio.read                         servo-thread
addf bb_gpio.write                        servo-thread""".format(pru_name=pru_name))

        p, n = pin_unpack(enable_pin)
        enable_data = {'enable_hader': p, 'enable_pin': n}
        hal_data.replace('Enable', """
net enable iocontrol.0.user-enable-out => iocontrol.0.emc-enable-in
net enable => bb_gpio.p{enable_hader}.out-{enable_pin}
net tool-prep-loop iocontrol.0.tool-prepare => iocontrol.0.tool-prepared
net tool-change-loop iocontrol.0.tool-change => iocontrol.0.tool-changed""".format(**enable_data))

        p, n = pin_unpack(klapan_pin)
        kalpan_data = {'klapan_hader': p, 'klapan_pin': n}
        hal_data.replace('Klapan_net',
                         "net klapan bb_gpio.p{klapan_hader}.out-{klapan_pin} <= motion.digital-out-00".format(
                             **kalpan_data))

        # generate nets for buttons
        button_nets = StringIO.StringIO()
        for button_net, pin in buttons_map.iteritems():
            is_invert = 'TRUE'
            hader, pin_n = pin_unpack(pin)
            source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
            dest_pin_n = button_net[1:]
            if len(dest_pin_n) == 1:
                dest_pin_n = '0' + dest_pin_n
            dest_pin = 'motion.digital-in-{}'.format(dest_pin_n)
            button_nets.write('setp {}.invert {}\n'.format(source_pin, is_invert))
            button_nets.write('net {} {} => {}\n'.format(button_net, source_pin, dest_pin))
        hal_data.replace('button_nets', button_nets.getvalue())

        # generate nets for home
        home_nets = StringIO.StringIO()
        for home_net, (h, is_invert) in homemap.iteritems():
            hader, pin_n = pin_unpack(h)
            source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
            dest_pin = 'axis.{}.home-sw-in'.format(home_net[1:])
            home_nets.write('setp {}.invert {}\n'.format(source_pin, is_invert))
            home_nets.write('net {} {} => {}\n'.format(home_net, source_pin, dest_pin))
            # --
            din_debug_pin_pin = 'motion.digital-in-{}'.format(int(home_net[1:]) + 10)
            home_nets.write('net {} => {}\n'.format(home_net, din_debug_pin_pin))
            # --
        hal_data.replace('home_switches', home_nets.getvalue())

        # generate pru step generators
        for axis_n in range(axes_count):
            hal_data.append(generate_pru_config(axis_n, pru_name))

    is_hal_correct = False
    if os.path.isfile(hal_config_filename):
        with open(hal_config_filename, 'r') as existing_hal_config:
            m_e = hashlib.md5()
            m_e.update(existing_hal_config.read())
            m_n = hashlib.md5()
            m_n.update(hal_data.get())
            is_hal_correct = m_e.digest() == m_n.digest()
    if not is_hal_correct:
        with open(hal_config_filename, 'wb') as hal_config:
            hal_config.write(hal_data.get())


def generate_ini(is_sim):
    # ini
    config_template = RawMultiConfigParser()
    config_template.optionxform = str  # case sensitive
    config_template.read([config_template_filename])

    if is_sim:
        # sim
        config_template.set('DISPLAY', 'DISPLAY', 'axis')
        # config_template.set('DISPLAY', 'DISPLAY', 'linuxcncrsh')
        config_template.set('EMC', 'MACHINE', '3dp-napilenie-sim')
        config_template.set('HAL', 'HALFILE', ['core_sim.hal', hal_config_filename])
    else:
        # work
        # config_template.set('DISPLAY', 'DISPLAY', 'tkemc')
        config_template.set('DISPLAY', 'DISPLAY', 'linuxcncrsh')
        config_template.set('EMC', 'MACHINE', '3dp-napilenie')
        config_template.set('HAL', 'HALFILE', hal_config_filename)
    is_ini_correct = False
    new_config = StringIO.StringIO()
    config_template.write(new_config)
    if os.path.isfile(result_config_filename):
        with open(result_config_filename, 'r') as existing_config_file:
            m_e = hashlib.md5()
            m_e.update(existing_config_file.read())
            m_n = hashlib.md5()
            m_n.update(new_config.getvalue())
            is_ini_correct = m_e.digest() == m_n.digest()
    if not is_ini_correct:
        with open(result_config_filename, 'wb') as existing_config_file:
            existing_config_file.write(new_config.getvalue())

    return config_template


def cd():
    import os

    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)


def main():
    """
    Оопределить по uname на чем запуск
    1. Если не arm, то запускаем симуляцию. Физические кнопки заменет коно с кнопками, еще запущен halmeter
    2. Если arm, то запускаем полноцелнный режим. Физические кнопки подключены
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--sim', default=False, action='store_true')
    parser.add_argument('--remotegui', default=False, action='store_true')

    args = parser.parse_args()

    is_sim = args.sim or ('arm' not in cpuinfo.get_cpu_info()['raw_arch_string'])

    cd()

    config = generate_ini(is_sim)
    generate_hal(is_sim, config)

    def start_gui():
        subprocess.Popen([sys.executable, 'GtkClient/start_gui.py'])

    # start GUI if local
    if not args.remotegui:
        rh_main_loop.add_callback(start_gui)

    # for Rockhopper server
    my_rh_main(result_config_filename)


if __name__ == "__main__":
    main()
    print('Server stopped.')
    exit(0)
