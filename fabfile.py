#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from fabric.api import env, cd, roles, sudo, run
from fabric.contrib.project import rsync_project

env.roledefs['cnc'] = ['machinekit@192.168.7.2']


def production_env():
    """Окружение для продакшена"""
    env.user = 'machinekit'  # На сервере будем работать из под пользователя "git"
    env.project_folder = '/home/machinekit/machinekit/configs'
    env.project_root = os.path.join(env.project_folder, '3dp-napilenie')  # Путь до каталога проекта (на сервере)
    env.shell = '/bin/bash -c'  # Используем шелл отличный от умолчательного (на сервере)
    env.pip = 'pip'
    with open('.gitignore', 'r') as gitignore:
        env.rsync_excludes = list(gitignore.read().strip().split('\n'))
    env.rsync_excludes.append('.git', )
    env.rsync_excludes.append('Rockhopper')


@roles('cnc')
def pip_install():
    production_env()
    sudo('{pip} install --upgrade -r {filepath}'.format(pip=env.pip,
                                                        filepath=os.path.join(env.project_root, 'requirements.txt'),
                                                        user=env.user))


@roles('cnc')
def rsync():
    production_env()
    rsync_project(remote_dir=env.project_folder, exclude=env.rsync_excludes)
    run('rm position.txt || true')


@roles('cnc')
def dtc():
    production_env()
    with cd(env.project_root):
        sudo('./dtc.sh')
