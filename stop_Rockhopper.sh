#!/bin/bash

pgrep -f start.py | while read -r pid ; do
  echo "killing $pid"
  kill -9 $pid
done

pgrep -f start_gui.py | while read -r pid ; do
  echo "killing $pid"
  kill $pid
done